import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router'
import {NgbDate, NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { MapsAPILoader } from '@agm/core';
//import {} from '@types/googlemaps';
/// <reference path="<relevant path>/node_modules/@types/googlemaps/index.d.ts" />
import { ViewChild, ElementRef, NgZone, } from '@angular/core';
import { FormGroup, FormControl, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete/ngx-google-places-autocomplete.directive';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import {CRUDService} from '../crud.service';
@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  form: FormGroup;
  hoveredDate: NgbDate;
  fromDate: NgbDate;
  toDate: NgbDate;
  title:string;
  discription:string;

  @ViewChild('places') places: GooglePlaceDirective;
  @ViewChild('search' ) public searchElement: ElementRef;
    lat: number = -33.785809;
    lng: number = 151.121195;

  constructor(calendar: NgbCalendar, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private crud_service : CRUDService,  private router: Router) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 5);
  }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', Validators.compose([Validators.required])),
      discription: new FormControl('', Validators.compose([Validators.required])),
      place: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }


  public handleAddressChange(address: Address) {
    // console.log(address.geometry.location.lng());
    // console.log(address.geometry.location.lat());
     console.log(address.geometry.location.toJSON());
    // console.log(address.geometry.viewport.getNorthEast());
    this.lng = address.geometry.location.lng();
    this.lat  = address.geometry.location.lat();
}

onClickSubmit(form) {
 let eventData:any = this.form.value;
 eventData['lat'] = this.lat ;
 eventData['lng'] = this.lng ;
 eventData['start'] =  new Date(this.fromDate.year+'-'+this.fromDate.month+'-'+this.fromDate.day) ;
 eventData['end'] = this.toDate != null ? new Date(this.toDate.year+'-'+this.toDate.month+'-'+this.toDate.day) :  this.toDate;
 console.log(eventData['start']);
  this.crud_service.saveEventInfo(eventData).subscribe(
    data =>{
        let result = data;
        // if(result.success){
        //   alert('Event was saved successfully!');
        //  this.router.navigate(['index']);
        // }
        // if(result.fail){
        //   alert('Failed to save event!');
        // }
    } ,
    err => console.log(err)
  );
}
}
