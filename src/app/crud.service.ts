import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CRUDService {
  private apiUrl = environment.apiUrl;  
  constructor(private http: HttpClient) { }

  getContentJSON() {
    return this.http.get(this.apiUrl + 'events');
    // return this.http.get('assets/json/events.json');
  }

  saveEventInfo(eventObj){
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
      })
    };    
    let params = JSON.stringify(eventObj) ;
    return this.http.post(this.apiUrl + 'create', params, httpOptions);
  }
} 
