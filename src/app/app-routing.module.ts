import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventComponent } from './event/event.component';
import { ListComponent } from './list/list.component';
const routes: Routes = [
  {path: 'index', component : ListComponent},
  {path: 'event', component : EventComponent},
  {path: '', redirectTo:'/index' ,pathMatch :'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
