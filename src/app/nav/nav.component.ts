import { Component, OnInit } from '@angular/core';



declare var $ :any;
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
 

constructor() {

  }



  ngOnInit() {
     $('.navbar-nav>li>a').on('click', function(){
      $('.navbar-collapse').collapse('hide');
    });

  
  }


  logout() {
    sessionStorage.clear();
    
  }
}
